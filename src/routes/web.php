<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** @var Illuminate\Routing\Router $router */
$router = app('router');

$router->group(['namespace' => 'Front'], function() use ($router) {
    Route::get('/{any}', 'SpaController@index')->where('any', '.*');
});