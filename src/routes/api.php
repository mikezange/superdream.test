<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/** @var Illuminate\Routing\Router $router */
$router = app('router');

$router->group(['namespace' => 'Api','middleware' => 'api'], function() use ($router){
    $router->get('/people', 'PersonController@index');

    $router->group(['prefix' => 'auth'], function ()  use ($router){
        $router->post('login', 'AuthController@login');
        $router->post('logout', 'AuthController@logout');
        $router->post('refresh', 'AuthController@refresh');
        $router->post('me', 'AuthController@me');
    });
});
