<?php

use App\Entities\User;
use Illuminate\Database\Seeder;

class CreateUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Mike',
                'email' => 'mike.zange.1@gmail.com',
                'password' => bcrypt('logmein'),
            ],
            [
                'name' => 'André',
                'email' => 'andre@superdream.com',
                'password' => bcrypt('logmein'),
            ]
        ];

        foreach ($users as $user){
            User::create($user);
        }
    }
}
