<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Base\Eloquent\EloquentBaseRepository;
use App\Repositories\PersonRepository;

class EloquentPersonRepository extends EloquentBaseRepository implements PersonRepository
{
}