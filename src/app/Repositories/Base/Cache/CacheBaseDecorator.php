<?php

namespace App\Repositories\Base\Cache;

use Illuminate\Cache\Repository;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Base\BaseRepository;

abstract class CacheBaseDecorator implements BaseRepository
{
    /**
     * @var \App\Repositories\Base\BaseRepository
     */
    protected $repository;
    /**
     * @var Repository
     */
    protected $cache;
    /**
     * @var string The entity name
     */
    protected $entityName;
    /**
     * @var string The application locale
     */
    protected $locale;

    /**
     * @var int caching time
     */
    protected $cacheTime;

    public function __construct()
    {
        $this->cache = app(Repository::class);
        $this->cacheTime = app(ConfigRepository::class)->get('cache.time', 60);
        $this->locale = app()->getLocale();
    }

    public function queryBuilder(): Builder
    {
        return $this->repository->queryBuilder();
    }

    /**
     * @inheritdoc
     */
    public function all($columns = ['*'])
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.all", $this->cacheTime,
                function () use ($columns) {
                    return $this->repository->all($columns);
                }
            );
    }

    /**
     * @inheritdoc
     */
    public function find($id, $columns = ['*'])
    {
        $tagIdentifier = json_encode($columns);

        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.find.{$id}.{$tagIdentifier}", $this->cacheTime,
                function () use ($id, $columns) {
                    return $this->repository->find($id, $columns);
                }
            );
    }

    /**
     * @inheritdoc
     */
    public function findBySlug($slug)
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.find.{$slug}", $this->cacheTime,
                function () use ($slug) {
                    return $this->repository->findBySlug($slug);
                }
            );
    }

    /**
     * @inheritdoc
     */
    public function firstByAttributes(array $attributes)
    {
        $tagIdentifier = json_encode($attributes);

        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.find.{$tagIdentifier}", $this->cacheTime,
                function () use ($attributes) {
                    return $this->repository->firstByAttributes($attributes);
                }
            );
    }

    /**
     * @inheritdoc
     */
    public function allByAttributes(array $attributes, $orderBy = null, $sortOrder = 'asc')
    {
        $tagIdentifier = json_encode($attributes);

        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.find.{$tagIdentifier}.{$orderBy}.{$sortOrder}", $this->cacheTime,
                function () use ($attributes, $orderBy, $sortOrder) {
                    return $this->repository->allByAttributes($attributes, $orderBy, $sortOrder);
                }
            );
    }

    /**
     * @inheritdoc
     */
    public function has($relationship)
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.find.{$relationship}", $this->cacheTime,
                function () use ($relationship) {
                    return $this->repository->has($relationship);
                }
            );
    }

    /**
     * @inheritdoc
     */
    public function paginate($perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $tagIdentifier = json_encode($columns);

        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.find.{$perPage}.{$tagIdentifier}.{$pageName}.{$page}", $this->cacheTime,
                function () use ($perPage, $columns, $pageName, $page) {
                    return $this->repository->paginate($perPage, $columns, $pageName, $page);
                }
            );
    }

    /**
     * @inheritdoc
     */
    public function create($data)
    {
        $this->clearCache();
        return $this->repository->create($data);
    }

    /**
     * @inheritdoc
     */
    public function update($model, $data)
    {
        $this->clearCache();
        return $this->repository->update($model, $data);
    }

    /**
     * @inheritdoc
     */
    public function delete($entity)
    {
        $this->clearCache();
        return $this->repository->delete($entity);
    }

    /**
     * @inheritdoc
     */
    public function forceDelete($entity)
    {
        $this->clearCache();
        return $this->repository->forceDelete($entity);
    }

    /**
     * @inheritdoc
     */
    public function clearCache()
    {
        return $this->cache->tags($this->entityName)->flush();
    }

    public function truncate()
    {
        $this->clearCache();
        return $this->repository->truncate();
    }

    /**
     * @inheritdoc
     */
    public function newEntityInstance($attributes = [])
    {
        return $this->repository->newEntityInstance($attributes);
    }
}
