<?php

namespace App\Repositories\Cache;

use App\Repositories\Base\Cache\CacheBaseDecorator;
use App\Repositories\PersonRepository;

class CachePersonDecorator extends CacheBaseDecorator implements PersonRepository
{
    public function __construct(PersonRepository $repository)
    {
        parent::__construct();
        $this->entityName = 'person';
        $this->repository = $repository;
    }
}
