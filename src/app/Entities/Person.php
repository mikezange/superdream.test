<?php

namespace App\Entities;

use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use Encryptable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'legacy_id',
        'first_name',
        'last_name',
        'job_title',
        'website',
        'email',
        'email_hash',
        'gender',
        'lat',
        'long',
        'address',
        'ip_address',
        'ordering',
    ];

    public $encryptable = [
        'first_name',
        'last_name',
        'email',
        'gender',
        'lat',
        'long',
        'address',
        'ip_address',
    ];

    protected $hidden = [
        'id',
        'legacy_id',
        'email_hash',
    ];

    protected $casts = [
        'ordering' => 'array'
    ];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = $value;
        $this->attributes['email_hash'] = hash(config('hashing.static_hash_algo'), strtolower(trim(decrypt($value))));
    }

}
