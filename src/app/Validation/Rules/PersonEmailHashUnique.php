<?php

namespace App\Validation\Rules;

use App\Repositories\PersonRepository;
use Illuminate\Contracts\Validation\ImplicitRule;

class PersonEmailHashUnique implements ImplicitRule
{
    /**
     * @var PersonRepository
     */
    private $userRepository;
    /**
     * @var null
     */
    private $ignore;

    public function __construct($id = null)
    {
        $this->ignore = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->userRepository = app()->make(PersonRepository::class);

        $hashedValue = hash(config('hashing.static_hash_algo'), $value);
        $exists = $this->userRepository->allByAttributes(["{$attribute}_hash" => $hashedValue]);

        if(count($exists) === 1 && $exists[0]->id === (int)$this->ignore){
            return true;
        }

        return count($exists) <= 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This :attribute already exists in the system';
    }
}
