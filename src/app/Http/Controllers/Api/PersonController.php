<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\PersonRepository;
use Illuminate\Cache\Repository;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    /**
     * Cache Repository
     */
    protected $cache;

    /**
     * App Locale
     */
    protected $locale;

    /**
     * @var PersonRepository
     */
    private $personRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
        $this->middleware('auth:api');
        $this->cache = app(Repository::class);
        $this->locale = app()->getLocale();
    }

    public function index(Request $request)
    {
        $count = $request->get('count') ?? 15;
        $sortBy = $request->get('sortBy') ?? "first_name";
        $page = $request->get('page') ?? 1;

        $params = request()->except(['page']);

        return $this->cache
                    ->tags(['person', 'global'])
                    ->remember("{$this->locale}.person.api.{$count}.{$sortBy}.{$page}", 60,
                        function () use ($sortBy, $count, $params) {
                            return $this->personRepository
                                        ->queryBuilder()
                                        ->orderBy("ordering->{$sortBy}")
                                        ->paginate($count)
                                        ->appends($params);
                        }
                    );
    }
}
