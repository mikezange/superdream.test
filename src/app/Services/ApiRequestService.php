<?php

namespace App\Services;


use App\Exceptions\ApiRequestService\NoUrlException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ApiRequestService
{
    /**
     * @param null $url
     *
     * @return string
     * @throws NoUrlException
     * @throws GuzzleException
     */
    public function get($url = null)
    {
        if(!$url){
            throw new NoUrlException("No URL Supplied");
        }

        return $this->makeRequest($url, 'GET');
    }

    /**
     * @param $url
     * @param string $method
     * @param array $params
     *
     * @return string
     * @throws GuzzleException
     */
    private function makeRequest($url, $method = "GET", $params = [])
    {
        $client = new Client();

        try{
            $res = $client->request($method, $url, $params);
        }catch (GuzzleException $e){
            throw $e;
        }

        return $res->getBody()->getContents();
    }
}