<?php

namespace App\Console\Commands;

use App\Exceptions\ApiRequestService\NoUrlException;
use App\Exceptions\InvalidJsonException;
use App\Repositories\PersonRepository;
use App\Services\ApiRequestService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;

class ImportPeople extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:people {--update} {--skip} {--truncate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports people from the feeds specified in config/feeds.php';

    /**
     * @var ApiRequestService
     */
    private $apiRequestService;

    /**
     * @var PersonRepository
     */
    private $personRepository;

    private $googleMapsKey;

    /**
     * Create a new command instance.
     *
     * @param ApiRequestService $apiRequestService
     * @param PersonRepository $personRepository
     */
    public function __construct(ApiRequestService $apiRequestService, PersonRepository $personRepository)
    {
        parent::__construct();

        $this->apiRequestService = $apiRequestService;
        $this->personRepository = $personRepository;
        $this->googleMapsKey = env('GOOGLEMAPS_API_KEY', '');
    }

    /**
     * Execute the import.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        if ($this->option('truncate')) {
            $this->info('Truncating data');
            $this->personRepository->truncate();
        }
        $this->info('Clearing cache');
        $this->personRepository->clearCache();

        $feeds = config('feeds.people');

        $feedData = [];

        foreach ($feeds as $feed) {
            try {
                $response = $this->apiRequestService->get($feed);
                $feedData[] = json_decode($response, true);

                if (json_last_error() !== 0) {
                    throw new InvalidJsonException("Feed contained invalid JSON, killing import");
                }
            } catch (\Exception $e) {
                $this->error($e->getMessage());
                $this->error("Feed: {$feed}");
                exit;
            }
        }

        $feedData = array_reduce($feedData, 'array_merge', []);

        $this->importData($feedData);
    }

    /**
     * Import the actual data
     *
     * @param $feedData
     *
     * @throws GuzzleException
     */
    private function importData($feedData)
    {
        $this->info("Importing data");
        $this->transformData($feedData);

        foreach ($feedData as $data) {
            $emailHash = hash(config('hashing.static_hash_algo'), strtolower(trim($data['email'])));

            $person = $this->personRepository->firstByAttributes(['email_hash' => $emailHash]);

            if ($person) {
                $this->handleExistingRecord($person, $data);
            } else {
                $this->personRepository->create($data);
            }
        }
    }

    /**
     * A nice place to add any transforms on the data before it gets imported.
     *
     * @param $data
     *
     * @throws GuzzleException
     */
    private function transformData(&$data)
    {
        foreach ($data as &$datum) {
            $this->makeIdLegacy($datum);
            $this->prefixWebAddress($datum);
            $this->reverseGeocode($datum);
        }

        $data = $this->addOrderingData($data);
    }

    /**
     * Make the records id the legacy id
     * Remove current id so we can use DB incremental id
     *
     * @param $data
     */
    private function makeIdLegacy(&$data)
    {
        $data['legacy_id'] = $data['id'];
        unset($data['id']);
    }

    /**
     * Prefix the URL with HTTPS if there is currently no scheme
     *
     * @param $data
     */
    private function prefixWebAddress(&$data)
    {
        $scheme = parse_url($data['website'], PHP_URL_SCHEME);
        if (empty($scheme)) {
            $data['website'] = 'https://' . ltrim($data['website']);
        }
    }

    /**
     * Creates ordering data so we can order the people without decrypting the data
     *
     * @param $data
     * @return array
     */
    private function addOrderingData($data)
    {
        $collection = collect($data);
        $this->addOrderBy($collection, ['first_name', 'last_name', 'email']);
        
        return $collection->toArray();
    }

    /**
     * Adds ordering data for a specific fields
     *
     * @param $collection
     * @param $fields
     */
    private function addOrderBy(&$collection, $fields)
    {
        foreach ($fields as $field) {
            $collection = $collection->sortBy($field)->values();

            $collection = $collection->map(function ($item, $key) use ($field) {
                $item['ordering'][$field] = $key;
                return $item;
            });
        }
    }

    /**
     * Give the user the ability to choose what to do with duplicated records
     *
     * @param $person
     * @param $data
     */
    private function handleExistingRecord($person, $data)
    {
        $choice = null;

        if (!$this->option('skip') && !$this->option('update')) {
            $question = "User exists: {$data['email']}, What should we do with the record?";
            $choice = $this->choice($question, ['update', 'skip'], "0");
        }

        if ($this->option('update') || $choice === "update") {
            $this->info('Updating record');
            $this->personRepository->update($person, $data);
        } else if($this->option('skip') || $choice === "skip"){
            $this->info('Skipping record');
        }
    }

    /**
     * @param $datum
     *
     * @throws GuzzleException
     */
    private function reverseGeocode(&$datum)
    {
        try {

            $response = $this->apiRequestService->get("https://maps.googleapis.com/maps/api/geocode/json?latlng={$datum['lat']},{$datum['long']}&key={$this->googleMapsKey}");

            $geocode = json_decode($response, true);

            if(isset($geocode['results'][0])){
                $datum['address'] = $geocode['results'][0]['formatted_address'];
            }else{
                $datum['address'] = "{address_not_found}";
            }

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
