<?php

namespace App\Providers;

use App\Entities\Person;
use App\Entities\User;
use App\Repositories\Cache\CacheRoleDecorator;
use App\Repositories\Cache\CachePersonDecorator;
use App\Repositories\Eloquent\EloquentPersonRepository;
use App\Repositories\Eloquent\EloquentRoleRepository;
use App\Repositories\Eloquent\EloquentUserRepository;
use App\Repositories\PersonRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Spatie\Permission\Models\Role;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindRepository(
            PersonRepository::class,
            Person::class,
            EloquentPersonRepository::class,
            CachePersonDecorator::class
        );
    }

    private function bindRepository($repository, $entity, $eloquentRepository, $cacheDecorator)
    {
        $entity = $this->app->make($entity);
        $eloquentRepository = $this->app->makeWith($eloquentRepository, ['entity' => $entity]);

        $this->app->singleton($repository, function () use ($entity, $eloquentRepository, $cacheDecorator) {
            $repository = $eloquentRepository;

            if (! Config::get('app.cache')) {
                return $repository;
            }

            return $this->app->makeWith($cacheDecorator, ['repository' => $repository]);
        });
    }
}
