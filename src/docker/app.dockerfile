FROM php:7.2.8-fpm

RUN apt-get update && apt-get install -y zlib1g-dev libmemcahced-dev mysql-client --no-install-recommends && pecl install memcached && docker-php-ext-install pdo_mysql && docker-php-ext-install memcached