<?php

namespace Tests\Feature\Api;

use App\Entities\User;
use Tests\TestCase;

class PeopleApiTest extends TestCase
{
    /**
     * A check to see if the people api loads
     *
     * @return void
     */
    public function testPeopleApiReturnsResponse()
    {
        $user = auth('api')->login(User::find(1));
        $response = $this->get('/api/people');
        $response->assertStatus(200);
    }

    public function testPeopleApiReturnsJson()
    {
        $user = auth('api')->login(User::find(1));
        $response = $this->get('/api/people');
        $response->assertJsonStructure();
    }

    public function testPeopleApiLimitReturnsCorrect()
    {
        $user = auth('api')->login(User::find(1));
        $response = $this->get('/api/people?count=5');

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(5, count($data['data']));
    }
}
