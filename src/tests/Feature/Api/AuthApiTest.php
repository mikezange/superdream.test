<?php

namespace Tests\Feature\Api;

use App\Entities\User;
use Tests\TestCase;

class AuthApiTest extends TestCase
{
    /**
     * A check to see if the people api loads
     *
     * @return void
     */
    public function testLoginReturnAccessToken()
    {
        $response = $this->postJson('/api/auth/login', [
            'email' => 'mike.zange.1@gmail.com',
            'password' => 'logmein',
        ]);

        $response->assertJsonStructure([
            'access_token',
        ]);
    }
}
