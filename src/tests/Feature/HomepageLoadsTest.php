<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomepageLoadsTest extends TestCase
{
    /**
     * A check to see if the home page loads
     *
     * @return void
     */
    public function testHomepageLoads()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
