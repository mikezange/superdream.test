
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
import App from './components/App'
import Map from './components/Map'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faAt, faBrowser, faServer } from '@fortawesome/pro-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faAt, faBrowser, faServer);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;


Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'map',
      component: Map
    },
  ],
});

const app = new Vue({
  el: '#app',
  components: { App },
  router,
});