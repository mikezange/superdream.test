##Instructions:

####Docker

__Pre-requisites:__ Docker 1.10 or later installed on host machine

run `docker-compose up` from `./src` folder.

Visit `http://localhost:8080/`


####Valet

__Pre-requisites:__ valet, memcached, mysql, php7.2 installed

From `./src` folder run the following commands

`valet link people`

`valet secure people`

`composer install`

`yarn install && yarn run production` or `yarn run dev`

Modify the .env file to use your DB connection details

`php artisan migrate`

`php artisan db:seed`

`php artisan import:people`

#####There are various options for the import, if no options are passed the user is asked for an action.

`--truncate` will truncate all data in the people table and re-import

`--skip` any duplicate records will be skipped

`--update` any duplicate records will be updated with the most recent record

Visit `https://people.test/`

